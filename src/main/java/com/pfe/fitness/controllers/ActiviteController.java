package com.pfe.fitness.controllers;

import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pfe.fitness.entities.Activite;
import com.pfe.fitness.services.ActiviteService;

@RestController
@RequestMapping("/activite") // localhost:8087/activite
public class ActiviteController {

	@Autowired
	private ActiviteService activiteService;
	
	@Autowired  
	ServletContext context;


	@GetMapping(path = "/all") //localhost:8087/activite/all
	public List<Activite> getAllActivite() {
		return activiteService.getAllActivite();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Activite>  findActiviteById (@PathVariable Long id) {
		Activite activite = activiteService.findActiviteById(id);
		if (activite==null) {
			return new ResponseEntity<Activite>(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity<Activite>(activite, HttpStatus.OK);
		}
	}
	
	@PostMapping
	public Activite createActivite(@RequestBody Activite activite) {
		return activiteService.createActivite(activite);
	}
	
	@PutMapping
	public Activite updateActivite(@RequestBody Activite activite) {
		return activiteService.updateActivite(activite);
				
	}
    @DeleteMapping(path= "/{id}") 
	public void deleteActivite(@PathVariable Long id) {
		 activiteService.deleteActivite(id);
	}
	@DeleteMapping("/delete/{NomE}")
	public String deleteActivite(@PathVariable String NomA)
    {	
		 return activiteService.deleteActivite(NomA);
    }
}
