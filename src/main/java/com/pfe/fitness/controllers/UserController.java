package com.pfe.fitness.controllers;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pfe.fitness.entities.ERole;
import com.pfe.fitness.entities.User;
import com.pfe.fitness.repository.UserRepository;
import com.pfe.fitness.services.UtilisateurServices;



@RestController
@RequestMapping("/utilisateur")
public class UserController {

	@Autowired
	private UtilisateurServices utilisateurService;
	@GetMapping (path = "/all")
	public List<User> getAllUtilisateurs() {
		return utilisateurService.getAllUtilisateurs();
	}
	
	// localhost:8080/utilisateur/1
	@GetMapping(path = "/{id}") 
	public ResponseEntity<User> findUtilisateurById(@PathVariable Long id) {
		User utilisateur = utilisateurService.findUtilisateurById(id);

		if (utilisateur == null) {
			return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<User>(utilisateur, HttpStatus.OK);
		}
	}
	
	@PostMapping
	public User createUtilisateur(@RequestBody User utilisateur) {
		return utilisateurService.createUtlisateur(utilisateur);
	}
	
	@PutMapping
	public User updateUtilisateur(@RequestBody User utilisateur) {
		return utilisateurService.updateUtlisateur(utilisateur);
	} 

	@DeleteMapping(path = "/{id}") 
	public void deleteUtilisateur(@PathVariable Long id) {
		utilisateurService.deleteUtlisateur(id);
	}
	
	@GetMapping(path = "/findByRoleName/{name}") // localhost:8087/utilisateur/findByRoleName/ADMIN
	public ResponseEntity<List<User>> findUtilisateurByRole(@PathVariable ERole name) {
		List<User> utilisateurs = utilisateurService.findByRolesName(name);

		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<User>>(utilisateurs, HttpStatus.OK);
		}
	}
	
	
	
	
	

	

}

