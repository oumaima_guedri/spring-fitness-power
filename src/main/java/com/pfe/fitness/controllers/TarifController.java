package com.pfe.fitness.controllers;

import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pfe.fitness.entities.Tarif;
import com.pfe.fitness.services.TarifService;

@RestController
@RequestMapping("/tarif") // localhost:8087/tarif
public class TarifController {

	@Autowired
	private TarifService tarifService;
	
	@Autowired  
	ServletContext context;


	@GetMapping(path = "/all") //localhost:8087/tarif/all
	public List<Tarif> getAllTarif() {
		return tarifService.getAllTarif();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Tarif>  findActiviteById (@PathVariable Long id) {
		Tarif tarif = tarifService.findTarifById(id);
		if (tarif==null) {
			return new ResponseEntity<Tarif>(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity<Tarif>(tarif, HttpStatus.OK);
		}
	}
	
	@PostMapping
	public Tarif createActivite(@RequestBody Tarif tarif) {
		return tarifService.createTarif(tarif);
	}
	
	@PutMapping
	public Tarif updateTarif(@RequestBody Tarif tarif) {
		return tarifService.updateTarif(tarif);
				
	}
    @DeleteMapping(path= "/{id}") 
	public void deleteTarif(@PathVariable Long id) {
		 tarifService.deleteTarif(id);
	}
	@DeleteMapping("/delete/{nom}")
	public String deleteTarif(@PathVariable String nom)
    {	
		 return tarifService.deleteTarif(nom);
    }
}
