package com.pfe.fitness.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pfe.fitness.entities.ERole;
import com.pfe.fitness.entities.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	 
	 //Auth,register repo
	    Optional<User> findByEmail(String email);
	    /*Boolean existsByUsername(String username);*/
		public boolean existsByprenom(String prenom);
		public boolean existsByEmail(String email);
		public List<User> findByNomAndPrenom(String nom , String prenom);
		User findByNom(String nom);
		public List <User>findByRolesName(ERole name);
		
		//public List<User> findByRoles(String name);

		

	     
	 
	 //Users methodes
	 /*
	    public List<Utilisateur> findByFirstName(String firstname);
		public Utilisateur findByEmail(String email);
		public List<Utilisateur> findByFirstNameAndLastName(String firstname, String lastname);
		@Query("SELECT u FROM Utilisateur u WHERE u.firstName = ?1 OR u.lastName = ?2 ")
		public List<Utilisateur> findByFirstNameAndLastNameWithJPQL(String firstname, String lastname);
		
		@Query("SELECT u FROM Utilisateur u WHERE u.firstname LIKE :myFirstname OR u.lastname LIKE :mylastname")
		public List<Utilisateur> findByFirstNameAndLastNameWithJPQLWithNamedParameters
		(@Param(value = "myFirstname") String firstName,@Param(value = "myLastname") String lastname);
		public List<Utilisateur> findByRolesName(ERole name);
		*/
}
